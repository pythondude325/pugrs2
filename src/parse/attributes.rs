use nom::branch::alt;
use nom::bytes::complete::{escaped_transform, tag, take_till1};
use nom::character::complete::{multispace1, none_of};
use nom::combinator::{map, opt};
use nom::multi::{separated_list};
use nom::sequence::{delimited, pair, preceded};
use nom::IResult;

use super::value::value;

#[derive(Clone)]
enum AttributeValue {
    String(String),
    Boolean(bool),
}

fn attribute_name(s: &str) -> IResult<&str, &str> {
    // From https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
    take_till1(
        |c: char| c == ' ' || c == '"' || c == '\'' || c == '>' || c == '/' || c == '=' || c == ')'
    )(s)
}

fn quoted_string(s: &str) -> IResult<&str, String> {
    alt((
        delimited(
            tag("\""),
            escaped_transform(
                none_of("\\\""),
                '\\',
                alt((
                    value(tag("\\"), "\\"),
                    value(tag("\""), "\""),
                    value(tag("n"), "\n"),
                )),
            ),
            tag("\""),
        ),
        delimited(
            tag("\'"),
            escaped_transform(
                none_of("\\\'"),
                '\\',
                alt((
                    value(tag("\\"), "\\"),
                    value(tag("\'"), "\'"),
                    value(tag("n"), "\n"),
                )),
            ),
            tag("\'"),
        )
    ))(s)
}

fn boolean(s: &str) -> IResult<&str, bool> {
    alt((
        value(tag("true"), true),
        value(tag("false"), false),
    ))(s)
}


pub fn parse_attributes(s: &str) -> IResult<&str, Vec<(String, String)>> {
    map(separated_list(
        pair(opt(tag(",")), multispace1),
        pair(
            attribute_name,
            opt(preceded(
                tag("="),
                alt((
                    map(quoted_string, AttributeValue::String),
                    map(boolean, AttributeValue::Boolean),
                )),
            )),
        ),
    ), |v| {
        v.iter().filter_map(|(k, v)| {
            let k = k.to_string();
            match v {
                // `key='value'` => ("key", "value")
                Some(AttributeValue::String(s)) => Some((k.clone(), s.clone())),
                // `key=true` => ("key", "key")
                Some(AttributeValue::Boolean(true)) => Some((k.clone(), k.clone())),
                // `key=false` => ()
                Some(AttributeValue::Boolean(false)) => None,
                // `key` => ("key", "key")
                None => Some((k.clone(), k.clone())),
            }
        }).collect()
    })(s)
}