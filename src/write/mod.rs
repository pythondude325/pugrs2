
use crate::structures::*;
use std::io::{Cursor, Write};
use xml::writer::{EmitterConfig, EventWriter, Result, XmlEvent};
fn write_element(mut writer: &mut EventWriter<impl Write>, element: &Element) -> Result<()> {
    writer.write({
        let mut start_tag = XmlEvent::start_element(element.tag_name.as_str());

        for (name, value) in element.attributes.iter() {
            start_tag = start_tag.attr(name.as_str(), value.as_str());
        }

        start_tag
    })?;

    for child in element.children.iter() {
        write_node(&mut writer, child)?;
    }

    writer.write(XmlEvent::end_element())?;

    Ok(())
}

fn write_node(mut writer: &mut EventWriter<impl Write>, node: &Node) -> Result<()> {
    match node {
        Node::Element(e) => write_element(&mut writer, e),
        Node::Text(s) => writer.write(XmlEvent::characters(s)),
        Node::Comment(s) => writer.write(XmlEvent::comment(s)),
        Node::UnbufferedNode => Ok(()),
    }
}

pub fn write_tree(tree: &[Node]) -> Result<String> {
    let mut cursor = Cursor::new(Vec::<u8>::new());
    let mut writer = EmitterConfig::new()
        .autopad_comments(false)
        .write_document_declaration(false)
        .create_writer(&mut cursor);

    for node in tree.iter() {
        write_node(&mut writer, node)?;
    }

    Ok(String::from_utf8(cursor.into_inner()).unwrap())
}