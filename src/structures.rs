
#[derive(Clone, Debug, PartialEq)]
pub enum Node {
    Element(Element),
    Text(String),
    Comment(String),
    UnbufferedNode,
}

impl Node {

    pub fn text(s: &str) -> Node {
        Node::Text(s.to_string())
    }

    pub fn comment(s: &str) -> Node {
        Node::Comment(s.to_string())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Element {
    pub tag_name: String,
    pub attributes: Vec<(String, String)>,
    pub children: Vec<Node>,
}

impl Element {
    pub fn new(tag_name: &str) -> Element {
        Element {
            tag_name: tag_name.to_string(),
            attributes: Vec::new(),
            children: Vec::new(),
        }
    }

    pub fn builder(tag_name: &str) -> ElementBuilder {
        ElementBuilder(Element::new(tag_name))
    }
}

pub struct ElementBuilder(Element);

impl ElementBuilder {
    pub fn attribute<S>(mut self, key: S, value: S) -> Self
    where
        S: Into<String>,
    {
        self.0.attributes.push((key.into(), value.into()));
        self
    }

    pub fn attributes<S>(mut self, attributes: Vec<(S, S)>) -> Self
    where
        S: Into<String>,
    {
        for attr in attributes {
            self = self.attribute(attr.0, attr.1);
        }

        self
    }

    pub fn child(mut self, node: Node) -> Self {
        self.0.children.push(node);
        self
    }

    pub fn children(mut self, children: Vec<Node>) -> Self {
        for child in children {
            self = self.child(child);
        }

        self
    }

    pub fn unwrap(self) -> Element {
        self.0
    }
}